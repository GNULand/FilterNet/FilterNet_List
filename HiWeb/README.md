for Status:
(✅ Mean Accessable | ❌ Mean Inaccessible | ❔ Mean Not Avalibale)


# [HiWeb](http://www.hiweb.ir/)
| Website or Application | Protocol | URL or Address | Date(YYYY/MM/DD) | Status | Description |
| :------: | :------: | :------: | :------: | :------: | :------: |
| DuckDuckGo | HTTPS | duckduckgo.com | 2017/12/19 | ❌ | ❔ |
| DuckDuckGo | HTTPS | dgg.gg | 2017/12/19 | ❌ | ❔ |
| Twitter | HTTPS | twitter.com | 2017/12/19 | ❌ | ❔ |
| Facebook | HTTPS | facebook.com | 2017/12/19 | ❌ | ❔ |
| YouTube | HTTPS | youtube.com | 2017/12/19 | ✅ | ❔ |
| SoundCloud | HTTPS | soundcloud.com | 2017/12/19 | ✅ | ❔ |
| Spotify | HTTPS | spotify.com | 2017/12/19 | ❌ | ❔ |
| ASL19 | HTTPS | asl19.org | 2017/12/20 | ❌ | ❔ |
| Dolate Bahar | HTTP | dolatebahar.com | 2017/12/20 | ❌ | ❔ |
| OYAN NEWS | HTTP | oyannews.com | 2017/12/20 | ❌ | ❔ |
| PATREON | HTTP | patreon.com | 2017/12/20 | ❌ | ❔ |
| SOCCERWAY | HTTP | soccerway.com | 2017/12/20 | ❌ | ❔ |
| fOURSQUARE | HTTPS | foursquare.com | 2017/12/20 | ❌ | ❔ |
| SOCCERWAY | HTTPS | fr.soccerway.com | 2017/12/20 | ❌ | ❔ |
| livestream | HTTPS | livestream.com | 2017/12/20 | ❌ | ❔ |
| greprepclub | HTTPS | greprepclub.com | 2017/12/20 | ❌ | ❔ |
| Swift | HTTPS | swift.org | 2017/12/20 | ❌ | ❔ |
| Subscene | HTTPS | subscene.com | 2017/12/20 | ❌ | ❔ |
| Musixmatch | HTTPS | musixmatch.com | 2017/12/20 | ❌ | ❔ |
| meetup | HTTPS | meetup.com | 2017/12/20 | ❌ | ❔ |
| Vimeo  | HTTPS | vimeo.com | 2017/12/20 | ❌ | ❔ |
| swarm | HTTPS | swarmapp.com | 2017/12/20 | ❌ | ❔ |
| Quora | HTTPS | quora.com | 2017/12/20 | ❌ | ❔ |
| FlashScore | HTTPS | flashscore.com | 2017/12/20 | ❌ | ❔ |
| Keyboard Azad (کیبرد آزاد) | HTTPS | jadi.net | 2017/12/20 | ❌ | ❔ |
| Ganool | HTTPS | ganool.se | 2017/12/20 | ❌ | ❔ |
| SlideShare | HTTPS | slideshare.net | 2017/12/20 | ❌ | ❔ |
